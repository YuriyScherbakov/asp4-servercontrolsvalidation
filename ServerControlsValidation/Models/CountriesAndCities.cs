﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerControlsValidation.Models
{
    public class CountriesAndCities 
    {
        public static DataSet CountriesAndCitiesLoad(string serverMapPath)
        {
            DataSet dsCountriesAndCities = new DataSet("CountriesAndCities");

            DataTable dataTableCountries = new DataTable("Country");
            dataTableCountries.Columns.Add("CountryName",typeof(System.String));
            dataTableCountries.Columns.Add("CountryID",typeof(System.Int32));

            DataTable dataTableCities = new DataTable("City");
            dataTableCities.Columns.Add("CityName",typeof(System.String));
            dataTableCities.Columns.Add("CityID",typeof(System.Int32));
            dataTableCities.Columns.Add("CountryID",typeof(System.Int32));

            dataTableCountries.ReadXml(serverMapPath + @"\App_Data\CountriesAndCities.xml");
            dataTableCities.ReadXml(serverMapPath + @"\App_Data\CountriesAndCities.xml");

            
            dsCountriesAndCities.Tables.Add(dataTableCountries);
            dsCountriesAndCities.Tables.Add(dataTableCities);
            dsCountriesAndCities.AcceptChanges();

            return dsCountriesAndCities;
        }

    }
}
