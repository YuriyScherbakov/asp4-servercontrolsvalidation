﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace ServerControlsValidation
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        HtmlForm form;
        HtmlGenericControl divForm;
        Table TableRegistration;
        CompareValidator compareValidatorNickFirstName;
        CompareValidator compareValidatorFirstNameLastName;
        TextBox TextBoxNickname;
        RequiredFieldValidator RequiredFieldValidatorNickname;
        TextBox TextBoxFirstName;
        RequiredFieldValidator RequiredFieldValidatorFirstName;
        TextBox TextBoxLastName;
        RequiredFieldValidator RequiredFieldValidatorLastName;
        TextBox TextBoxBirthDay;
        ImageButton ImageButton;
        RequiredFieldValidator RequiredFieldValidatorBirthDay;
        RangeValidator RangeValidatorDate;
        TextBox TextBoxEmail;
        RequiredFieldValidator RequiredFieldValidatorEmail;
        RegularExpressionValidator RegularExpressionValidatorEmail;
        DropDownList DropDownListCountry;
        DropDownList DropDownListCity;
        Calendar calendar;
        Label labelError;
        Label labelText;
        Button ButtonClearAll;
        Button ButtonSubmit;
        ValidationSummary ValidationSummary;


        protected void Page_Init(object sender,EventArgs e)
        {
            this.form = new HtmlForm();
            this.Page.Controls.Add(this.form);
            this.divForm = new HtmlGenericControl();
            this.form.Controls.Add(divForm);

            #region TableRegistration

            this.TableRegistration = new Table();
            this.TableRegistration.ID = "TableRegistration";
            this.divForm.Controls.Add(this.TableRegistration);

            #region TableRowNickname 

            TableRow TableRowNickname = new TableRow();
            this.TableRegistration.Controls.Add(TableRowNickname);

            TableCell tableCellNickname_1 = new TableCell();
            tableCellNickname_1.Text = "Псевдоним";
            TableCell tableCellNickname_2 = new TableCell();

            TextBoxNickname = new TextBox();
            TextBoxNickname.ID = "TextBoxNickname";
            tableCellNickname_2.Controls.Add(TextBoxNickname);

            TableCell tableCellNickname_3 = new TableCell();
            RequiredFieldValidatorNickname = new RequiredFieldValidator();
            RequiredFieldValidatorNickname.ID = "RequiredFieldValidatorNickname";
            RequiredFieldValidatorNickname.ControlToValidate = "TextBoxNickname";
            RequiredFieldValidatorNickname.ErrorMessage = "Псевдоним обязателен";
            RequiredFieldValidatorNickname.Text = " *";
            RequiredFieldValidatorNickname.ForeColor = System.Drawing.Color.Red;

            tableCellNickname_3.Controls.Add(RequiredFieldValidatorNickname);

            TableRowNickname.Controls.Add(tableCellNickname_1);
            TableRowNickname.Controls.Add(tableCellNickname_2);
            TableRowNickname.Controls.Add(tableCellNickname_3);

            #endregion

            #region TableRowFirstName 

            TableRow TableRowFirstName = new TableRow();
            this.TableRegistration.Controls.Add(TableRowFirstName);

            TableCell tableCellFirstName_1 = new TableCell();
            tableCellFirstName_1.Text = "Имя";
            TableCell tableCellFirstName_2 = new TableCell();

            TextBoxFirstName = new TextBox();
            TextBoxFirstName.ID = "TextBoxFirstName";
            tableCellFirstName_2.Controls.Add(TextBoxFirstName);

            TableCell tableCellFirstName_3 = new TableCell();

            RequiredFieldValidatorFirstName = new RequiredFieldValidator();
            RequiredFieldValidatorFirstName.ID = "RequiredFieldValidatorFirstName";
            RequiredFieldValidatorFirstName.ControlToValidate = "TextBoxFirstName";
            RequiredFieldValidatorFirstName.ErrorMessage = "Имя обязательно";
            RequiredFieldValidatorFirstName.Text = " *";
            RequiredFieldValidatorFirstName.ForeColor = System.Drawing.Color.Red;

            tableCellFirstName_3.Controls.Add(RequiredFieldValidatorFirstName);

            TableCell tableCellFirstName_4 = new TableCell();

            compareValidatorNickFirstName = new CompareValidator();
            compareValidatorNickFirstName.ControlToValidate = "TextBoxFirstName";
            compareValidatorNickFirstName.ErrorMessage = "Имя должно отличаться от псевдонима";
            compareValidatorNickFirstName.Font.Size = 8;
            compareValidatorNickFirstName.ForeColor = System.Drawing.Color.Red;
            compareValidatorNickFirstName.ControlToCompare = "TextBoxNickname";
            compareValidatorNickFirstName.Operator = ValidationCompareOperator.NotEqual;
            compareValidatorNickFirstName.Display = ValidatorDisplay.Dynamic;
            compareValidatorNickFirstName.Type = ValidationDataType.String;
            compareValidatorNickFirstName.SetFocusOnError = true;

            tableCellFirstName_4.Controls.Add(compareValidatorNickFirstName);


            TableRowFirstName.Controls.Add(tableCellFirstName_1);
            TableRowFirstName.Controls.Add(tableCellFirstName_2);
            TableRowFirstName.Controls.Add(tableCellFirstName_3);
            TableRowFirstName.Controls.Add(tableCellFirstName_4);
            #endregion

            #region TableRowLastName

            TableRow TableRowLastName = new TableRow();
            this.TableRegistration.Controls.Add(TableRowLastName);

            TableCell tableCellLastName_1 = new TableCell();
            tableCellLastName_1.Text = "Фамилия";

            TableCell tableCellLastName_2 = new TableCell();

            TextBoxLastName = new TextBox();
            TextBoxLastName.ID = "TextBoxLastName";
            tableCellLastName_2.Controls.Add(TextBoxLastName);

            TableCell tableCellLastName_3 = new TableCell();

            RequiredFieldValidatorLastName = new RequiredFieldValidator();
            RequiredFieldValidatorLastName.ID = "RequiredFieldValidatorLastName";
            RequiredFieldValidatorLastName.ControlToValidate = "TextBoxLastName";
            RequiredFieldValidatorLastName.ErrorMessage = "Фамилия обязательна";
            RequiredFieldValidatorLastName.Text = " *";
            RequiredFieldValidatorLastName.ForeColor = System.Drawing.Color.Red;

            tableCellLastName_3.Controls.Add(RequiredFieldValidatorLastName);

            TableCell tableCellLastName_4 = new TableCell();

            compareValidatorFirstNameLastName = new CompareValidator();
            compareValidatorFirstNameLastName.ControlToValidate = "TextBoxLastName";
            compareValidatorFirstNameLastName.Font.Size = 8;
            compareValidatorFirstNameLastName.ForeColor = System.Drawing.Color.Red;
            compareValidatorFirstNameLastName.ErrorMessage = "Фамилия должна отличаться от имени";
            compareValidatorFirstNameLastName.ControlToCompare = "TextBoxFirstName";
            compareValidatorFirstNameLastName.Operator = ValidationCompareOperator.NotEqual;
            compareValidatorFirstNameLastName.Display = ValidatorDisplay.Dynamic;
            compareValidatorFirstNameLastName.Type = ValidationDataType.String;
            compareValidatorFirstNameLastName.SetFocusOnError = true;

            tableCellLastName_4.Controls.Add(compareValidatorFirstNameLastName);


            TableRowLastName.Controls.Add(tableCellLastName_1);
            TableRowLastName.Controls.Add(tableCellLastName_2);
            TableRowLastName.Controls.Add(tableCellLastName_3);
            TableRowLastName.Controls.Add(tableCellLastName_4);

            #endregion

            #region TableRowBirthDay

            TableRow TableRowBirthDay = new TableRow();
            this.TableRegistration.Controls.Add(TableRowBirthDay);

            TableCell tableCellBirthDay_1 = new TableCell();
            tableCellBirthDay_1.Text = "Дата рождения";

            TableCell tableCellBirthDay_2 = new TableCell();

            TextBoxBirthDay = new TextBox();
            TextBoxBirthDay.ID = "TextBoxBirthDay";
            TextBoxBirthDay.Enabled = false;
            tableCellBirthDay_2.Controls.Add(TextBoxBirthDay);

            

            TableCell tableCellBirthDay_3 = new TableCell();

            RequiredFieldValidatorBirthDay = new RequiredFieldValidator();
            RequiredFieldValidatorBirthDay.ID = "RequiredFieldValidatorBirthDay";
            RequiredFieldValidatorBirthDay.ControlToValidate = "TextBoxBirthDay";
            RequiredFieldValidatorBirthDay.ErrorMessage = "Дата рождения обязателена";
            RequiredFieldValidatorBirthDay.Text = " *";
            RequiredFieldValidatorBirthDay.Display = ValidatorDisplay.Dynamic;
            RequiredFieldValidatorBirthDay.ForeColor = System.Drawing.Color.Red;

            tableCellBirthDay_3.Controls.Add(RequiredFieldValidatorBirthDay);

            TableCell tableCellBirthDay_4 = new TableCell();

            this.ImageButton = new ImageButton();
            this.ImageButton.CausesValidation = false;
            this.ImageButton.ImageUrl = "~/Img/calendar.png";
            this.ImageButton.Style.Add("Width","20px");
            this.ImageButton.Style.Add("Height","20px");
            this.ImageButton.Click += ImageButton_Click;

            tableCellBirthDay_4.Controls.Add(ImageButton);

            RangeValidatorDate = new RangeValidator();
            RangeValidatorDate.ControlToValidate = "TextBoxBirthDay";
            RangeValidatorDate.ErrorMessage = "Дата рождения должна быть диапазоне с 12/01/1960 по " + DateTime.Now.ToShortDateString();
            this.RangeValidatorDate.Type = ValidationDataType.Date;
            this.RangeValidatorDate.MaximumValue = DateTime.Now.ToShortDateString();
            this.RangeValidatorDate.MinimumValue = DateTime.Parse("12/01/1960").ToShortDateString();
            this.RangeValidatorDate.EnableClientScript = true;
            this.RangeValidatorDate.ForeColor = System.Drawing.Color.Red;
            this.RangeValidatorDate.Text = " !";


            tableCellBirthDay_4.Controls.Add(RangeValidatorDate);


            TableRowBirthDay.Controls.Add(tableCellBirthDay_1);
            TableRowBirthDay.Controls.Add(tableCellBirthDay_2);
            TableRowBirthDay.Controls.Add(tableCellBirthDay_3);
            TableRowBirthDay.Controls.Add(tableCellBirthDay_4);

            #endregion

            #region TableRowEmail

            TableRow TableRowEmail = new TableRow();
            this.TableRegistration.Controls.Add(TableRowEmail);

            TableCell tableCellEmail_1 = new TableCell();
            tableCellEmail_1.Text = "E-mail";
            TableCell tableCellEmail_2 = new TableCell();

            TextBoxEmail = new TextBox();
            TextBoxEmail.ID = "TextBoxEmail";
            tableCellEmail_2.Controls.Add(TextBoxEmail);

            TableCell tableCellEmail_3 = new TableCell();

            RequiredFieldValidatorEmail = new RequiredFieldValidator();
            RequiredFieldValidatorEmail.ID = "RequiredFieldValidatorEmail";
            RequiredFieldValidatorEmail.ControlToValidate = "TextBoxEmail";
            RequiredFieldValidatorEmail.ErrorMessage = "E-mail адрес обязателен";
            RequiredFieldValidatorEmail.Text = " *";
            RequiredFieldValidatorEmail.ForeColor = System.Drawing.Color.Red;

            tableCellEmail_3.Controls.Add(RequiredFieldValidatorEmail);

            TableCell tableCellEmail_4 = new TableCell();

            RegularExpressionValidatorEmail = new RegularExpressionValidator();
            RegularExpressionValidatorEmail.ControlToValidate = "TextBoxEmail";
            RegularExpressionValidatorEmail.Text = "Неправильно введен адрес E-mail";
            RegularExpressionValidatorEmail.ForeColor = System.Drawing.Color.Red;
            RegularExpressionValidatorEmail.Font.Size = 8;
            RegularExpressionValidatorEmail.ID = "RegularExpressionValidatorEmail";
            RegularExpressionValidatorEmail.ValidationExpression = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            RegularExpressionValidatorEmail.Display = ValidatorDisplay.Dynamic;
            RegularExpressionValidatorEmail.ErrorMessage = "Неправильно введен адрес E-mail";
            RegularExpressionValidatorEmail.SetFocusOnError = true;

            tableCellEmail_4.Controls.Add(RegularExpressionValidatorEmail);


            TableRowEmail.Controls.Add(tableCellEmail_1);
            TableRowEmail.Controls.Add(tableCellEmail_2);
            TableRowEmail.Controls.Add(tableCellEmail_3);
            TableRowEmail.Controls.Add(tableCellEmail_4);

            #endregion

            #region TableRowCountry 

            TableRow TableRowCountry = new TableRow();
            this.TableRegistration.Controls.Add(TableRowCountry);

            TableCell tableCellCountry_1 = new TableCell();
            tableCellCountry_1.Text = "Страна";

            TableCell tableCellCountry_2 = new TableCell();

            DropDownListCountry = new DropDownList();
            DropDownListCountry.ID = "DropDownListCountry";
            DropDownListCountry.AutoPostBack = true;
            DropDownListCountry.Style.Add("width","144px");
            tableCellCountry_2.Controls.Add(DropDownListCountry);

            TableRowCountry.Controls.Add(tableCellCountry_1);
            TableRowCountry.Controls.Add(tableCellCountry_2);

            #endregion

            #region TableRowCity 

            TableRow TableRowCity = new TableRow();
            this.TableRegistration.Controls.Add(TableRowCity);

            TableCell tableCellCity_1 = new TableCell();
            tableCellCity_1.Text = "Город";

            TableCell tableCellCity_2 = new TableCell();

            DropDownListCity = new DropDownList();
            DropDownListCity.ID = "DropDownListCity";
            DropDownListCity.Style.Add("width","144px");
            tableCellCity_2.Controls.Add(DropDownListCity);

            TableRowCity.Controls.Add(tableCellCity_1);
            TableRowCity.Controls.Add(tableCellCity_2);

            #endregion

            #endregion TableRegistration

           


            #region Calendar

            calendar = new Calendar();
            calendar.ID = "Calendar";
            calendar.SelectionMode = CalendarSelectionMode.Day;
            calendar.Visible = false;
            calendar.BackColor = System.Drawing.Color.Silver;
            calendar.SelectionChanged += Calendar_SelectionChanged;
            this.form.Controls.Add(calendar);

            #endregion

            labelText = new Label();
            this.labelText.Text = "Все поля необходимо заполнить";
            this.form.Controls.Add(new LiteralControl("<br />"));
            this.form.Controls.Add(this.labelText);
            this.labelText.ForeColor = System.Drawing.Color.Blue;


            #region Buttons

            this.form.Controls.Add(new LiteralControl("<br />"));
            this.form.Controls.Add(new LiteralControl("<br />"));

            ButtonClearAll = new Button();
            ButtonClearAll.Text = "Очистить";
            ButtonClearAll.CausesValidation = false;
            ButtonClearAll.ID = "ButtonClearAll";
            ButtonClearAll.Click += ButtonClearAll_Click;
            this.form.Controls.Add(this.ButtonClearAll);

            ButtonSubmit = new Button();
            ButtonSubmit.Text = "Отправить";
            ButtonSubmit.Click += ButtonSubmit_Click;
            this.form.Controls.Add(this.ButtonSubmit);

            #endregion

            labelError = new Label();
            this.labelError.Text = "Ошибки валидации:";
            this.form.Controls.Add(new LiteralControl("<br />"));
            this.form.Controls.Add(new LiteralControl("<br />"));
            this.form.Controls.Add(this.labelError);

            this.form.Controls.Add(new LiteralControl("<br />"));

            ValidationSummary = new ValidationSummary();
            this.form.Controls.Add(this.ValidationSummary);
        }
        
        private void ButtonSubmit_Click(object sender,EventArgs e)
        {
            Page.Validate();
            if ( IsValid )
            {
                this.labelError.Text = "Спасибо валидация прошла успешно!";

            }
        }

        private void ButtonClearAll_Click(object sender,EventArgs e)
        {
            this.labelError.Text = "Ошибки валидации:";
            TextBoxNickname.Text = "";
            TextBoxFirstName.Text = "";
            TextBoxLastName.Text = "";
            TextBoxBirthDay.Text = "";
            TextBoxEmail.Text = "";
        }

        void FillDropDownListCity()
        {
            this.DropDownListCity.Items.Clear();

            for ( int i = 0; i < ( (DataSet)Application ["CountriesAndCities"] ).Tables ["City"].Rows.Count; i++ )
            {
                DataRow city = ( (DataSet)Application ["CountriesAndCities"] ).Tables ["City"].Rows [i];
                if ( city ["CountryID"].ToString() == this.DropDownListCountry.SelectedValue )
                {
                    ListItem li = new ListItem(city ["CityName"].ToString());
                    this.DropDownListCity.Items.Add(li);
                }

            }
        }
        protected void Page_Load(object sender,EventArgs e)
        {

            if ( !IsPostBack )
            {
               
                this.DropDownListCountry.DataTextField = "CountryName";
                this.DropDownListCountry.DataValueField = "CountryID";
                this.DropDownListCountry.DataSource = ( (DataSet)Application ["CountriesAndCities"] ).Tables ["Country"];
                this.DropDownListCountry.DataBind();

            }

            FillDropDownListCity();
        }
        
        protected void ImageButton_Click(object sender,ImageClickEventArgs e)
        {
            this.labelError.Text = "Ошибки валидации:";

            if ( this.calendar.Visible == true )
            {
                this.calendar.Visible = false;
            }
            else
            {
                this.calendar.Visible = true;

            }

        }

        protected void Calendar_SelectionChanged(object sender,EventArgs e)
        {
            this.TextBoxBirthDay.Text = this.calendar.SelectedDate.ToShortDateString();
            this.calendar.Visible = false;
            this.RangeValidatorDate.Validate();
        }
    }
}