﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace ServerControlsValidation
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            Application ["CountriesAndCities"] =
                Models.CountriesAndCities.CountriesAndCitiesLoad(
                    System.Web.Hosting.HostingEnvironment.MapPath("/"));
            
        }
    }
}